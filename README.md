# Computer Architecture - Lab Exercises (2020-2021)

# Credits
Grigoris Pavlakis <grigpavl@ece.auth.gr>

# Description

This repository contains the answers to all lab exercises,
along with all relevant data (reports, `gem5` configuration files,
result files and source code)
for the Computer Architecture course at AUTh ECE, as taught in 
the Winter semester of 2020.

# Repository Organization

All exercises pertinent to each lab are inside the respective
folder (i.e. for the first lab exercise check the `lab1` folder).